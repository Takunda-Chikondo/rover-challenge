import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String [] boundary_coordinates, coordinates;
        int x_boundary, y_boundary, x, y;

        boundary_coordinates = input.nextLine().split(" ");
        x_boundary = Integer.parseInt(boundary_coordinates[0]);
        y_boundary = Integer.parseInt(boundary_coordinates[1]);

        coordinates = input.nextLine().split(" ");
        x = Integer.parseInt(String.valueOf(coordinates[0].charAt(0)));
        y = Integer.parseInt(String.valueOf(coordinates[0].charAt(1)));
        String orientation = coordinates[1];

        String commands  = input.nextLine();

        Grid grid = new Grid(x_boundary, y_boundary);
        Rover rover = new Rover(x, y, orientation, commands, grid);
        rover.start();
    }
}
