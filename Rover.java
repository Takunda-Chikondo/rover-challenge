import java.util.HashMap;

public class Rover {
    private int x, y, cardinal_index;
    private String commands;
    private Grid grid;
    private String [] cardinal_points = new String [] {"W", "N", "E", "S"};

    // constructor to initialize the rover
    public Rover(int x, int y, String direction, String commands, Grid grid){

        this.x = x;
        this.y = y;
        this.cardinal_index = get_cardinal_index(direction);
        this.commands = commands;
        this.grid = grid;
    }

    public int get_x(){
        return x;
    }

    public int get_y(){
        return y;
    }

    public String get_direction() {
        return cardinal_points[cardinal_index];
    }

    // move the rover in a forward direction
    public boolean move(){
        if(get_direction().equals("N")){
            if(y >= grid.get_y_boundary()){
                return false;
            }
            y++;
        }
        else if(get_direction().equals("E")){
            if(x == grid.get_x_boundary()){
                return false;
            }
            x++;
        }
        else if(get_direction().equals("S")){
            if(y <= 0){
                return false;
            }
            y--;
        }
        else{
            if(x <= 0){
                return false;
            }
            x--;
        }
        return true;
    }

    public void rotate_clockwise(){
        cardinal_index++;
        cardinal_index = cardinal_index > 3 ? 0 : cardinal_index;
    }

    public void rotate_anticlockwise(){
        cardinal_index--;
        cardinal_index = cardinal_index < 0 ? 3 : cardinal_index;
    }

    // the the list of commands sent to the rover and prints the final position to the console.
    public void start(){
        for(int i=0; i<commands.length(); i++){
            if(String.valueOf(commands.charAt(i)).equals("M")){
                if(!move()){
                    System.out.println("could not move to the specified location");
                    System.out.println("Last recorded location ");
                    break;
                }
            }
            else if(String.valueOf(commands.charAt(i)).equals("L")){
                rotate_anticlockwise();
            }
            else{
                rotate_clockwise();
            }
        }
        System.out.println(x + " " + y + " " + get_direction());
    }

    private int get_cardinal_index(String direction){
        for(int i = 0; i < cardinal_points.length; i++){
            if(cardinal_points[i].equals(direction))
                return i;
        }
        // error code for if the direction is invalid
        return -1;
    }
}
