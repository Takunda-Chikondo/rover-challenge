The program is run by running the program in an IDE and entering the input as specified in the document line by line.

The program was broken into 3 classes, namely a driver (main) class used to run the program, a rover class that is used to simulate the rover that keeps track of the rover location, direction, rotation and its coordinates on the grid. A grid class was used to simulate the grid and keep track of the boundaries of the grid to ensure that the rover does not go out of bounds. 

The sepearation of the code into these classes was done to make the code more maintainable as well as making the code easier to unit test.

Code was tested for correctness by the use of unit testing (junit 4). The unit tests were designed to test the intialization of the classes as well as the functionality of the classes including some known boundary cases such a the rover going out of bounds and when the carddinal points array wraps around etc. It was assumed that he rover boundaries are always from the orgin going into the positive direction. 

The testing was done with 77% code coverage.

For the test cases please refer to the Testclass.java. The sample input specified in the brief was also used as a test case.

8 10
12 E
MMLMRMMRRMML