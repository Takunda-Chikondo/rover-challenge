import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestClass {
    @Test
    public void grid_init_test(){
        Grid grid = new Grid(8,10);
        assertEquals(8, grid.get_x_boundary());
        assertEquals(10, grid.get_y_boundary());
    }

    @Test
    public void rover_init(){
        Grid grid = new Grid(8,10);
        Rover rover = new Rover(1, 2, "E", "", grid);
        assertEquals(1, rover.get_x());
        assertEquals(2, rover.get_y());
        assertEquals("E", rover.get_direction());
    }

    @Test
    public void rotate_clockwise_test(){
        Grid grid = new Grid(8,10);
        Rover rover = new Rover(1, 2, "E", "", grid);
        rover.rotate_clockwise();
        assertEquals("S", rover.get_direction());

        //boundary case
        //testing when the cardinal points array wraps around
        rover.rotate_clockwise();
        rover.rotate_clockwise();
        assertEquals("N", rover.get_direction());
    }

    @Test
    public void rotate_anticlockwise_test(){
        Grid grid = new Grid(8,10);
        Rover rover = new Rover(1, 2, "E", "", grid);
        rover.rotate_anticlockwise();
        assertEquals("N", rover.get_direction());

        //boundary case
        //testing when the cardinal points array wraps around
        rover.rotate_anticlockwise();
        assertEquals("W", rover.get_direction());
    }

    @Test public void move_test(){
        Grid grid = new Grid(8,10);
        Rover rover = new Rover(2, 2, "N", "", grid);
        rover.move();
        assertEquals(2, rover.get_x());
        assertEquals(3, rover.get_y());

        rover = new Rover(2, 2, "E", "", grid);
        rover.move();
        assertEquals(3, rover.get_x());
        assertEquals(2, rover.get_y());

        rover = new Rover(2, 2, "S", "", grid);
        rover.move();
        assertEquals(2, rover.get_x());
        assertEquals(1, rover.get_y());

        rover = new Rover(2, 2, "W", "", grid);
        rover.move();
        assertEquals(1, rover.get_x());
        assertEquals(2, rover.get_y());
    }

    @Test
    public void boundary_test(){

        // tests the boundary in the positive direction
        Grid grid = new Grid(2,2);
        Rover rover = new Rover(0, 0, "N", "", grid);
        rover.move();
        rover.move();
        rover.move();
        assertEquals(0, rover.get_x());
        assertEquals(2, rover.get_y());


        //test that the rover coordinates are not negative
        //it is assume the grid coordinates are always positive
        rover = new Rover(0, 1, "W", "", grid);
        rover.move();
        rover.move();
        rover.move();
        assertEquals(0, rover.get_x());
        assertEquals(1, rover.get_y());
    }
}