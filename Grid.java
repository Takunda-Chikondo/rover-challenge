public class Grid {
    private int x_boundary, y_boundary;

    public Grid(int x, int y){
        x_boundary = x;
        y_boundary = y;
    }

    public int get_x_boundary(){
        return x_boundary;
    }

    public int get_y_boundary(){
        return y_boundary;
    }


}
